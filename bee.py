import sys
import ntpath
import numpy as np
import glob         # Usefull to process all the images.
import pandas as pd # Loading of coordinates files.
import time

# IO
import matplotlib.pyplot as plt

# Image processing
from skimage import feature
from skimage import measure
from skimage import morphology as morpho
from skimage import filters

def extract_veins(bee):
    '''
    Extract the veins of the wings.

    Args:
        bee: image in rgb

    Returns:
        The pattern of the veins.
    '''
    gray = bee.mean(axis=2).astype('uint8')

    # Reduces the influence of the hairs (??) on the wings.
    gray = filters.gaussian(gray, 7)

    bw = gray < filters.threshold_yen(gray)

    # Segments the image into connected components.
    blobs_labels = measure.label(bw, connectivity=1)

    # Seach for the biggest one, it should be the wing's veins.
    # Components 0 is ignored as it isthe background.
    counts = np.bincount(blobs_labels[blobs_labels != 0].reshape(-1))
    biggest_object = np.argmax(counts)
    veins = blobs_labels == biggest_object

    veins = morpho.remove_small_holes(veins, area_threshold=400)

    # Only keep the pattern of the veins.
    skel = morpho.skeletonize(veins)

    return skel

def remove_straight_lines(veins):
    '''
    Find the almost straight line starting from the top or bottom of the image.
        The line must always move on the y axis but we tolerate 5 left/right move.

    Args:
        veins: A binary image with lines.

    Returns:
        A binary image with the straight lines removed.
    '''

    straight_lines = np.zeros(veins.shape)
    todo = []

    def search(from_bot):
        while len(todo):
            cur_x, cur_y, n_horizontal = todo.pop()

            # Outside the image
            if cur_y < 0 or cur_y > veins.shape[0]:
                continue

            # Aleready process
            if straight_lines[cur_y, cur_x]:
                continue

            straight_lines[cur_y, cur_x] = True

            # It is still possible to make a horizontal move ?
            if n_horizontal:
                # Check if we can go left or right
                if cur_x > 1 and veins[cur_y, cur_x - 1]:
                    todo.append((cur_x - 1, cur_y, n_horizontal - 1))

                if cur_x + 1 < veins.shape[1] and veins[cur_y, cur_x + 1]:
                    todo.append((cur_x + 1, cur_y, n_horizontal - 1))

            # Go up or down
            cur_y = cur_y - 1 if from_bot else cur_y + 1

            # Check if we can go left, right or just above
            if cur_x > 1 and (veins[cur_y, cur_x - 1]):
                todo.append((cur_x - 1, cur_y, n_horizontal))

            if veins[cur_y, cur_x]:
                todo.append((cur_x, cur_y, n_horizontal))

            if cur_x + 1 < veins.shape[1] and veins[cur_y, cur_x + 1]:
                todo.append((cur_x + 1, cur_y, n_horizontal))

    # Get all the lines starting from the bottom ...
    y, x = veins.shape
    start = np.where(veins[y - 50, :])[0]

    for s in start:
        todo.append((s, y - 50, 20))

    # ... and explore them (check if the line exist and copy it in `straight_lines`).
    search(True)

    # Same thngs but for the lines from the top.
    start = np.where(veins[50, :])[0]
    for s in start:
        todo.append((s, 50, 20))
    search(False)

    # Remove lines crossing the image.
    labels = measure.label(straight_lines)
    n_labels = labels.max()

    for i in range(1, n_labels + 1):
        mask = labels == i

        if mask[50:,].any() and mask[y - 50:,].any():
            veins = np.logical_xor(straight_lines == i, veins)  

    return veins

def remove_small_seg(veins):
    ''' Remove the  small connected components. '''

    # Segments the image into connected components.
    blobs_labels = measure.label(veins, connectivity=2)

    # Seach for the biggest one, it should be the wing's veins.
    # Components 0 is ignored as it isthe background.
    counts = np.bincount(blobs_labels[blobs_labels != 0].reshape(-1))
    biggest_object = np.argmax(counts)
    veins = blobs_labels == biggest_object
    return veins

def pyramid_kernel():
    ''' A 5x5 pyramid shape kernel. Usefull for detecting the intersections. '''
    kernel = np.ones(5*5).reshape((5,5))
    kernel[1:-1,1:-1] = 2
    kernel[2,2] = 3
    return kernel

def find_intersects(veins):
    '''
    For each pixel, compute a signature composed of the
        sum of the 5x5 neighboring pixels according a gaussian kernel.

        Let's see how lines and intersection impact the signature with 3x3 kernel:

        kernel = [[1, 1, 1],
                  [1, 2, 1],
                  [1, 1, 1]]

        No line    A line but    A centered line   An intersection
                  not centered       (1)               (2)

         . . .        x x x          . . .             x . x
         . . .        . . .          x x x             . x .
         . . .        . . .          . . .             x . .
        sig = 0    sig = 3         sig = 4           sig = 5

        The worst intersection possible (2) has a signature of 5 and the best line
        possible (1) has only 4. By aplying a threshold at 5, we should only get the
        intersection.
        In this small example, we used a 3x3 kernel but for a better reliabily we will
        use a 5x5 kernel. By the same methods as above, we obtains a threshold at 12.

    Args:
        veins: A binary image with straights lines.

    Returns:
        A binary image with only the intersections.
    '''
    sigs = filters.edges.convolve(veins.astype('int'), pyramid_kernel())
    return sigs > 12

def image_to_points_list(inter, min_dist):
    '''
    Get the coordinates of the detected intersection. If multiple pixels are too
        close (the eucledian distance is less than `min_dist`), we only keep one.

    Args:
        inter: A binary image
        min_dist: The minimum distance between two points.

    Returns:
        A tuple (y,x) of arrays representings the coordinates of the intersections.
    '''

    coords_y, coords_x = np.where(inter)
    coords_y = coords_y.reshape((coords_y.shape[0], 1)).astype(float)
    coords_x = coords_x.reshape((coords_x.shape[0], 1)).astype(float)

    dist = np.sqrt(np.power(coords_x - coords_x.swapaxes(0, 1), 2) + np.power(coords_y - coords_y.swapaxes(0, 1), 2))
    np.fill_diagonal(dist, np.inf)

    idx = dist.argmin()
    sx = dist.shape[0]

    nb_points = coords_x.shape[0]

    while dist[idx//sx, idx%sx] < min_dist and dist[idx//sx, idx%sx] < np.inf and nb_points > 1:
        point_to_remove = idx // sx
        dist[point_to_remove,:] = np.inf
        dist[idx%sx, point_to_remove] = np.inf

        coords_x[point_to_remove] = np.nan
        coords_y[point_to_remove] = np.nan

        idx = dist.argmin()

        nb_points -= 1

    return coords_y[~np.isnan(coords_y)].astype(int), coords_x[~np.isnan(coords_x)].astype(int)

def remove_bad_intersection(veins, coords, min_segment):
    '''
    Remove all intersection with a segment smaller than min_segment.

    Args:
        veins: The veins of the wings (a binary image).
        coords: The coordinates of the intersection (a tuple of array)
        min_segment : The mimal size of a segment (usefull to remove artifact and noise detected as segment).

    Returns:
        A tuple (y,x) of arrays representings the good coordinates of the intersections.
    '''

    sy, sx = veins.shape

    def check_lenght(x, y, dir_x, dir_y):
        # Check if the segment starting from (x,y) with a aproximante
        # direction of (dir_x, dir_y) is greater than `min_segment`.
        length = 0

        while length < min_segment:

            length += 1
            found = False

            # Iterate on all possible directions ...
            for i in range(-1, 2):
                for j in range(-1, 2):
                    # ... but remove the null vector, ...
                    if i == 0 and j == 0:
                        continue

                    # ... checks the bounds, ...
                    if x + i < 0 or x + i >= sx or y + j < 0 or y + j >= sy:
                        continue

                    # ... and compute the dot product. If it
                    # below 0: the direction (i,j) is not
                    # is the same direction as the original
                    # one, skip it.
                    if dir_x * i + dir_y * j < 0:
                        continue

                    # Then check if the segment continue in this direction.
                    if veins[y + j, x + i]:
                        y += j
                        x += i
                        found = True
                        break

                if found:
                    break

            # The segment does not continue.
            if not found:
                return False

        # The segment is large enough.
        return True

    res = ([], [])

    # Check for each coordinates ...
    for i in range(len(coords[0])):
        y, x = coords[0][i], coords[1][i]

        nb_correct_segment = 0

        # ... for each directions ...
        for i in range(-1, 2):
            for j in range(-1, 2):
                # ... when the direction is not the null vector ...
                if i == 0 and j == 0:
                    continue

                # ... and a segment is possible (we are still in the bounds) ...
                if x + i < 0 or x + i >= sx or y + j < 0 or y + j >= sy:
                    continue

                # ... if there is a segment from the pixel next of (x,y) in the direction (i,j) ...
                if not veins[y + j, x + i]:
                    continue

                # ... and it is greater than `min_dist`.
                if check_lenght(x + i, y + j, j, i):
                    nb_correct_segment += 1

        # We consider there is an intersection only if there is more thant 3 segments.
        if nb_correct_segment >= 3:
            res[0].append(y)
            res[1].append(x)

    return res

def detect_bee_intersects(bee):
    ''' Detects the intersections of the veins in a bee wing. '''
    veins = extract_veins(bee)
    veins = remove_straight_lines(veins)
    veins = remove_small_seg(veins)

    inter = find_intersects(veins)

    coords = image_to_points_list(inter, 50)
    coords = remove_bad_intersection(veins, coords, 100)

    return coords


def save_to_csv(name, points_list):
    f = open(name, 'w')
    for i in range(len(points_list[0])):
        f.write(str(points_list[0][i]) + ',' + str(points_list[1][i]) + '\n')
    f.close()

def process_folder(path, out='res'):
    imgs_path = sorted(glob.glob(path + '/*.jpg'))

    for p_img in imgs_path:
        print('Process', p_img)

        img = plt.imread(p_img)

        coords = detect_bee_intersects(img)

        save = out + '/' + ntpath.basename(p_img)[:-3] + 'csv'
        save_to_csv(save, coords)
        print('Saved under', save, '\n')

if __name__ == '__main__':
    if len(sys.argv) == 3:
        process_folder(sys.argv[1], sys.argv[2])
    if len(sys.argv) == 2:
        process_folder(sys.argv[1], 'RESULTS')
    else:
        print('Usage: ./intersect.py INPUT_FOLDER [OUTPUT_FOLDER]')
        sys.exit(1)

