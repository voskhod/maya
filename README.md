# Maya

Maya consist in a python script capable of detecting the veins intersections
of a bee wing. This is usefull to classify the species of a bee from only one
image of its wings. There is also a jupyter notebook to demonstrate the
procedure.

## Usage

* Dependecy installation `pip install -r requirement.txt`.
* Run the script on a directoty or an image: `bee.py INPUT [OUTPUT]`.
